/* eslint-disable import/no-unassigned-import */

const COMMAND_ALIASES = {
	pack: ['webpack', 'encore'],
	analyze: ['stats', 'analyzer'],
	lint: ['xo', 'test'],
	browsers: ['browserslist'],
};

function log(runtimeConfig, ...args) {
	if(runtimeConfig.outputJson) {
		return;
	}

	console.log('[WEBTOOL]', ...args);
}

function logStats(runtimeConfig, stats) {
	if(runtimeConfig.outputJson) {
		process.stdout.write(JSON.stringify(stats.toJson()));
	}
}

async function packCommand(argv, cwd) {
	const WebpackBuilder = require('../lib/webpack/builder.js');
	const WebpackRunner = require('../lib/webpack/runner.js');

	const builder = WebpackBuilder.fromArgv(argv, cwd);
	builder.configureEnv();
	const webpackConfig = await builder.build();

	if(argv.inspect || argv.i) {
		process.stdout.write(require('util').inspect(webpackConfig, {depth: 50}));
		return 0;
	}

	const runner = new WebpackRunner(webpackConfig);

	if(builder.runtimeConfig.useDevServer) {
		await runner.serve({
			host: builder.runtimeConfig.devServerHost,
			port: builder.runtimeConfig.devServerPort,
			silent: builder.runtimeConfig.outputJson,
		});
		return 0;
	}

	if(argv.watch) {
		if(!builder.runtimeConfig.outputJson) {
			log(builder.runtimeConfig, 'Watching enabled');
		}

		runner.watch(stats => logStats(builder.runtimeConfig, stats));

		return 0;
	}

	const stats = await runner.run();
	logStats(builder.runtimeConfig, stats);
	return stats.hasErrors() ? 1 : 0;
}

async function lintCommand(argv, cwd) {
	const Linter = require('../lib/linter.js');
	const linter = Linter.fromArgv(argv, cwd);

	if(argv.inspect || argv.i) {
		process.stdout.write(require('util').inspect(await linter.buildConfig(), {depth: 50}));
		return 0;
	}

	const result = await linter.lint();
	process.stdout.write(result.report);
	return result.success ? 0 : 1;
}

async function browserslistCommand(argv, cwd) {
	const ConfigLoader = require('./config-loader.js');
	const guessBrowserlist = require('browserslist');
	const configLoader = new ConfigLoader(cwd, (argv.mode || argv.m) === 'production');
	configLoader.configureEnv();
	const config = await configLoader.load();
	const browsers = guessBrowserlist(config.browserslist, {path: cwd, env: argv.e || argv.env});
	process.stdout.write(browsers.join('\n') + '\n');
	return 0;
}

function getCanonicalCommandName(command) {
	for(const possibleAlias in COMMAND_ALIASES) {
		if(!Object.prototype.hasOwnProperty.call(COMMAND_ALIASES, possibleAlias)) {
			continue;
		}

		if(possibleAlias === command || COMMAND_ALIASES[possibleAlias].includes(command)) {
			return possibleAlias;
		}
	}

	return command;
}

async function main(rawArgv, cwd) {
	const argv = require('yargs-parser')(rawArgv);
	const command = argv._.shift();
	let rc = 1;

	switch(getCanonicalCommandName(command)) {
		case 'analyze':
			process.argv = ['', 'webtool ' + command, ...rawArgv.slice(rawArgv.indexOf(command) + 1)];
			require('webpack-bundle-analyzer/lib/bin/analyzer.js');
			rc = undefined;
			break;
		case 'pack':
			rc = await packCommand(argv, cwd);
			break;
		case 'lint':
			rc = await lintCommand(argv, cwd);
			break;
		case 'browsers':
			rc = await browserslistCommand(argv, cwd);
			break;
		default:
			console.error('Unknown command: ' + command);
			console.error('Usage: $ webtool <command>');
			console.error('\nValid commands:');
			for(const possibleAlias in COMMAND_ALIASES) {
				if(!Object.prototype.hasOwnProperty.call(COMMAND_ALIASES, possibleAlias)) {
					continue;
				}

				console.error(`\t${possibleAlias.padEnd(8)} (aliases: ${COMMAND_ALIASES[possibleAlias].join(', ')})`);
			}
	}

	return rc;
}

module.exports = main;
