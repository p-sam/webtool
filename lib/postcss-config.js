const PostCSSPresetEnv = require('postcss-preset-env');
const PostCSSUse = require('postcss-use');
const doiuse = require('doiuse');

function createPostCSSConfig({browserslist, prependPlugins, isProduction}) {
	const plugins = [
		new PostCSSUse({modules: '*'}),
		new PostCSSPresetEnv({browsers: browserslist}),
		isProduction && doiuse({browsers: browserslist}),
	];

	if(prependPlugins) {
		return {plugins: [...prependPlugins, plugins]};
	}

	return {plugins};
}

module.exports = createPostCSSConfig;
