const COMMAND_ALIASES = {
	pack: ['webpack', 'encore'],
	analyze: ['stats', 'analyzer'],
	lint: ['test'],
	browsers: ['browserslist']
};

function getCanonicalCommandName(command) {
	for(const possibleAlias in COMMAND_ALIASES) {
		if(!Object.prototype.hasOwnProperty.call(COMMAND_ALIASES, possibleAlias)) {
			continue;
		}

		if(possibleAlias === command || COMMAND_ALIASES[possibleAlias].includes(command)) {
			return possibleAlias;
		}
	}

	return command;
}

async function inspect(value) {
	process.stdout.write(require('util').inspect(value, {depth: 50}));
}

async function packCommand(argv, cwd) {
	const builder = require('../lib/webpack/builder.js').fromArgv(argv, cwd);
	builder.configureEnv();
	const webpackConfig = await builder.build();

	if(argv.inspect || argv.i) {
		inspect(webpackConfig);
		return 0;
	}

	const WebpackRunner = require('../lib/webpack/runner.js');
	const runner = new WebpackRunner(webpackConfig, builder.encore.runtimeConfig);

	if(runner.useDevServer) {
		await runner.serve();
		return 0;
	}

	if(argv.watch) {
		await runner.watch();
		return 0;
	}

	if(!await runner.run()) {
		return 1;
	}

	return 0;
}

async function lintCommand(argv, cwd) {
	const linter = require('../lib/linter.js').fromArgv(argv, cwd);

	if(argv.inspect || argv.i) {
		inspect(await linter.buildConfig());
		return 0;
	}

	const result = await linter.lint();
	process.stdout.write(result.report);
	return result.success ? 0 : 1;
}

async function browserslistCommand(argv, cwd) {
	const ConfigLoader = require('./config-loader.js');

	const configLoader = new ConfigLoader(cwd, (argv.mode || argv.m) === 'production');
	configLoader.configureEnv();
	const config = await configLoader.load();

	const browsers = require('browserslist')(config.browserslist, {path: cwd, env: argv.e || argv.env});
	process.stdout.write(browsers.join('\n') + '\n');

	return 0;
}

async function analyzeCommand(command, rawArgv) {
	process.argv = ['node', 'webtool ' + command, ...rawArgv.slice(1)];
	require('webpack-bundle-analyzer/lib/bin/analyzer.js');
}

async function main(rawArgv, cwd) {
	const argv = require('yargs-parser')(rawArgv);
	const command = argv._.shift();
	let rc = 1;

	switch(getCanonicalCommandName(command)) {
		case 'pack':
			rc = await packCommand(argv, cwd);
			break;
		case 'lint':
			rc = await lintCommand(argv, cwd);
			break;
		case 'browsers':
			rc = await browserslistCommand(argv, cwd);
			break;
		case 'analyze':
			rc = await analyzeCommand(command, rawArgv);
			break;
		default:
			console.error('Unknown command: ' + command);
			console.error('Usage: $ webtool <command>');
			console.error('\nValid commands:');

			for(const possibleAlias in COMMAND_ALIASES) {
				if(!Object.prototype.hasOwnProperty.call(COMMAND_ALIASES, possibleAlias)) {
					continue;
				}

				console.error(`\t${possibleAlias.padEnd(8)} (aliases: ${COMMAND_ALIASES[possibleAlias].join(', ')})`);
			}
	}

	return rc;
}

module.exports = main;
