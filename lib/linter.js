const stylelint = require('stylelint');
stylelint.NoFilesFoundError = require('stylelint/lib/utils/noFilesFoundError.js');
stylelint.preset = require('../presets/stylelint.js');

const xo = require('@d3vc/webtool-xo');
xo.preset = require('../presets/xo.js');

const ConfigLoader = require('./config-loader.js');

const parseArrayOption = n => (Array.isArray(n) ? n : [n]).filter(Boolean);

function getStylelintFormatter(type) {
	if(!type || type === 'pretty') {
		return require('stylelint-formatter-pretty');
	}

	return type;
}

class Linter {
	constructor(input, options, cwd) {
		this.configLoader = new ConfigLoader(cwd);
		this.options = options;
		this.cwd = cwd;

		this.input = input;
	}

	async buildConfig() {
		const webtoolConfig = await this.configLoader.load();

		const linterConfig = {
			xo: xo.mergeOptions(null, {
				reset: ['extends', 'envs'],
				usePackageJson: false
			}, xo.preset),
			style: {
				...stylelint.preset,
				ignoreFiles: xo.DEFAULT_IGNORES
			}
		};

		if(webtoolConfig.configureXO) {
			await webtoolConfig.configureXO(linterConfig.xo);
		}

		if(webtoolConfig.configureStylelint) {
			await webtoolConfig.configureStylelint(linterConfig.style);
		}

		linterConfig.xo = xo.mergeOptions(linterConfig.xo, {
			fix: this.options.fix,
			cwd: this.cwd,
			ignores: [...this.options.ignores, ...this.options.js.ignores],
			extends: this.options.js.extends,
			plugins: this.options.js.plugins,
			extensions: this.options.js.extensions,
			envs: this.options.js.envs,
			format: this.options.format
		});

		linterConfig.style = {
			...linterConfig.style,
			fix: Boolean(this.options.fix),
			cwd: this.cwd
		};

		for(const [key, values] of Object.entries(this.options.css)) {
			if(!linterConfig.style[key]) {
				linterConfig.style[key] = [];
			}

			linterConfig.style[key].push(...values);
		}

		if(this.options.ignores) {
			linterConfig.style.ignores.push(...this.options.ignores);
		}

		return linterConfig;
	}

	static fromArgv(argv, cwd) {
		return new Linter(argv._, {
			fix: Boolean(argv.fix),
			ignores: parseArrayOption(argv.ignore),
			format: argv.format || xo.DEFAULT_FORMATTER,
			js: {
				ignores: parseArrayOption(argv.jsIgnore),
				extends: parseArrayOption(argv.jsExtend),
				plugins: parseArrayOption(argv.jsPlugin),
				extensions: parseArrayOption(argv.jsExtension),
				envs: parseArrayOption(argv.jsEnv)
			},
			css: {
				ignores: parseArrayOption(argv.cssIgnore),
				extends: parseArrayOption(argv.cssExtend),
				processors: parseArrayOption(argv.cssProcessor),
				extensions: parseArrayOption(argv.cssExtension),
				plugins: parseArrayOption(argv.cssPlugin)
			}
		}, cwd);
	}

	async lint() {
		const linterConfig = await this.buildConfig();

		let cssReport;
		try {
			const cssFiles = await xo.glob.files(this.input, linterConfig.style);

			cssReport = await stylelint.lint({
				files: cssFiles,
				config: {
					...linterConfig.style,
					overrides: xo.glob.filterOverrides(cssFiles, linterConfig.style.overrides)
				},
				formatter: getStylelintFormatter(linterConfig.style.format),
				fix: linterConfig.style.fix
			});
		} catch (error) {
			if(error instanceof stylelint.NoFilesFoundError) {
				cssReport = {errored: false, output: ''};
			} else {
				throw error;
			}
		}

		const jsReport = await xo.lintFiles(this.input, linterConfig.xo);

		return {
			success: jsReport.success && !cssReport.errored,
			report: [jsReport.output, cssReport.output].filter(Boolean).join('\n') + '\n'
		};
	}
}

module.exports = Linter;
