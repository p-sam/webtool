const path = require('path');
const xo = {
	linter: require('xo'),
	preset: require('../presets/xo.js'),
};
const stylelint = {
	linter: require('stylelint'),
	formatters: Object.assign({
		pretty: require('stylelint-formatter-pretty'),
	}, require('stylelint/lib/formatters')),
	NoFilesFoundError: require('stylelint/lib/utils/noFilesFoundError'),
	preset: require('../presets/stylelint.js'),
};
const deepmerge = require('deepmerge');
const globby = require('globby');
const ConfigLoader = require('./config-loader.js');

const parseArrayOption = n => (Array.isArray(n) ? n : [n]).filter(Boolean);

const DEFAULT_FORMATTER = 'pretty';

class Linter {
	constructor(input, options, cwd) {
		this.configLoader = new ConfigLoader(cwd);
		this.options = options;
		this.cwd = cwd;

		this.input = input;
	}

	async buildConfig() {
		const webtoolConfig = await this.configLoader.load();

		const linterConfig = {
			xo: deepmerge(xo.preset, {}),
			stylelint: deepmerge(stylelint.preset, {}),
		};

		if(webtoolConfig.configureXO) {
			webtoolConfig.configureXO(linterConfig.xo);
		}

		if(webtoolConfig.configureStylelint) {
			webtoolConfig.configureStylelint(linterConfig.stylelint);
		}

		linterConfig.xo = deepmerge(linterConfig.xo, {
			fix: this.options.fix,
			quiet: this.options.quiet,
			cwd: this.cwd,
			ignores: [...this.options.ignores, ...this.options.js.ignores],
			extends: this.options.js.extends,
			plugins: this.options.js.plugins,
			extensions: this.options.js.extensions,
			envs: this.options.js.envs,
		});

		linterConfig.stylelint = deepmerge(linterConfig.stylelint, {
			fix: Boolean(this.options.fix),
			extends: this.options.css.extends,
			extensions: this.options.css.extensions,
			ignores: [...this.options.ignores, ...this.options.css.ignores],
			processors: this.options.css.processors,
			plugins: this.options.css.plugins,
			cwd: this.cwd,
		});

		linterConfig.formatters = {
			name: this.options.format,
			xo: xo.linter.getFormatter(this.options.format),
			stylelint: stylelint.formatters[this.options.format] || '- formatter not found -',
		};

		return linterConfig;
	}

	static fromArgv(argv, cwd) {
		return new Linter(argv._, {
			fix: Boolean(argv.fix),
			quiet: Boolean(argv.quiet),
			ignores: parseArrayOption(argv.ignore),
			format: argv.format || DEFAULT_FORMATTER,
			js: {
				ignores: parseArrayOption(argv.jsIgnore),
				extends: parseArrayOption(argv.jsExtend),
				plugins: parseArrayOption(argv.jsPlugin),
				extensions: parseArrayOption(argv.jsExtension),
				envs: parseArrayOption(argv.jsEnv),
			},
			css: {
				ignores: parseArrayOption(argv.cssIgnore),
				extends: parseArrayOption(argv.cssExtend),
				processors: parseArrayOption(argv.cssProcessor),
				extensions: parseArrayOption(argv.cssExtension),
				plugins: parseArrayOption(argv.cssPlugin),
			},
		}, cwd);
	}

	async globFiles(patterns, {ignores, extensions, cwd}) {
		const files = await globby(patterns.length === 0 ? [`**/*.{${extensions.join(',')}}`] : patterns, {ignore: ignores, gitignore: true, cwd});
		return files
			.filter(file => extensions.includes(path.extname(file).slice(1)))
			.map(file => path.resolve(cwd, file));
	}

	async lint() {
		const linterConfig = await this.buildConfig();

		let cssReport;
		try {
			cssReport = await stylelint.linter.lint({
				files: await this.globFiles(this.input, {
					ignores: linterConfig.stylelint.ignores,
					extensions: linterConfig.stylelint.extensions,
					cwd: linterConfig.stylelint.cwd,
				}),
				config: linterConfig.stylelint,
				formatter: linterConfig.formatters.stylelint,
				fix: linterConfig.stylelint.fix,
			});
		} catch (error) {
			if(error instanceof stylelint.NoFilesFoundError) {
				cssReport = {errored: false, output: ''};
			} else {
				throw error;
			}
		}

		const jsReport = await xo.linter.lintFiles(this.input, linterConfig.xo);
		if(linterConfig.xo.fix) {
			xo.linter.outputFixes(jsReport);
		}

		return {
			success: jsReport.errorCount === 0 && !cssReport.errored,
			report: [linterConfig.formatters.xo(jsReport.results), cssReport.output].filter(Boolean).join('\n') + '\n',
		};
	}
}

module.exports = Linter;
