const fs = require('fs/promises');
const path = require('path');

function assertType(value, expectedType, label = 'value') {
	const actualType = typeof(value);
	if(actualType !== expectedType) {
		throw new Error(`${label} has type '${actualType}'. Expected type: '${expectedType}'`);
	}
}

class ConfigLoader {
	constructor(cwd, isProduction) {
		this.isProduction = Boolean(isProduction);
		this.cwd = cwd || process.cwd();
		this.configPath = this.absolutePathFor('./webtool.config.js');
		this.pkgPath = this.absolutePathFor('./package.json');
	}

	absolutePathFor(p) {
		return path.resolve(this.cwd, p);
	}

	async fileExists(p) {
		try {
			await fs.access(p);
			return true;
		} catch {
			return false;
		}
	}

	configureEnv() {
		process.env.NODE_ENV = this.isProduction ? 'production' : 'development';
	}

	async load() {
		let pkg = {};

		const config = {
			appEntry: null,
			configureEncore: null,
			configureWebpack: null,
			configureXO: null,
			configureStylelint: null,
			browserslist: null
		};

		if(await this.fileExists(this.pkgPath)) {
			pkg = require(this.pkgPath);
		}

		if(await this.fileExists(this.configPath)) {
			const webtoolConfig = require(this.configPath);

			switch(typeof webtoolConfig) {
				case 'string':
					config.appEntry = webtoolConfig;
					break;
				case 'function':
					Object.assign(config, await webtoolConfig());
					break;
				case 'object':
					Object.assign(config, webtoolConfig);
					break;
				default:
					throw new Error('Unexpected webtoolConfig format in ' + this.configPath);
			}
		}

		if((!config.appEntry && pkg.main)) {
			config.appEntry = pkg.main;
		}

		if(!config.browserslist && pkg.browserslist) {
			config.browserslist = pkg.browserslist;
		}

		for(const k of ['configureWebpack', 'configureEncore', 'configureXO', 'configureStylelint']) {
			if(config[k]) {
				assertType(config[k], 'function', k);
			}
		}

		config.pkg = pkg;

		return config;
	}
}

module.exports = ConfigLoader;
