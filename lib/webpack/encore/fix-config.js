const AssetsPlugin = require('assets-webpack-plugin');
const webpackManifest = require('@symfony/webpack-encore/lib/webpack-manifest-plugin/index.js');

const CONFIGURABLE_PLUGINS = {
	analyzer: () => require('webpack-bundle-analyzer').BundleAnalyzerPlugin,
	html: () => require('html-webpack-plugin'),
	eslintCompat: () => require('../plugin/eslint-compat-plugin.js')
};

async function fixEntryFilesPlugin(encore, plugin) {
	plugin.options.filename = 'asset_entrypoints.json';
	plugin.options.keepInMemory = true;

	if(encore._ext.entryFilesPluginCallback) {
		await encore._ext.entryFilesPluginCallback(plugin.options, encore.isProduction());
	}
}

async function fixConfig(encore, webpack) {
	// Fix baseURL being different in dev-server mode
	if(webpack.output.publicPath !== encore.publicPath) {
		webpack.output.publicPath = encore.publicPath;
	}

	encore._ext.eslintCompatPluginEnabled = encore._ext.compatCheckEnabled;

	for(let i = 0; i < webpack.plugins.length; i++) {
		const plugin = webpack.plugins[i];
		if(plugin instanceof AssetsPlugin && plugin.options.filename === 'entrypoints.json') {
			if(encore._ext.entryFilesPluginEnabled) {
				await fixEntryFilesPlugin(encore, plugin); // eslint-disable-line no-await-in-loop
			} else {
				webpack.plugins.splice(i, 1);
			}
		} else if(plugin instanceof webpackManifest.WebpackManifestPlugin && !encore._ext.manifestPluginEnabled) {
			webpack.plugins.splice(i, 1);
		}
	}

	for(const [key, classGetter] of Object.entries(CONFIGURABLE_PLUGINS)) {
		if(!encore._ext[`${key}PluginEnabled`]) {
			continue;
		}

		const WebpackPluginClass = await classGetter(); // eslint-disable-line no-await-in-loop

		const options = encore._ext[`${key}PluginOptions`] || {};
		const configFn = encore._ext[`${key}PluginCallback`];

		if(configFn) {
			await configFn(options, encore.isProduction()); // eslint-disable-line no-await-in-loop
		}

		webpack.plugins.push(new WebpackPluginClass(options));
	}

	if(encore._ext.netlifyRedirectProxies) {
		require('./netlify-redirects.js')(webpack, encore._ext.netlifyRedirectProxies);
	}
}

module.exports = fixConfig;
