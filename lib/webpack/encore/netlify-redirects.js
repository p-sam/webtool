const EmitFilePlugin = require('../plugin/emit-file-plugin.js');

function netlifyRedirectEntry(route, headers, force) {
	const entry = {
		from: route.prefix,
		to: route.target,
		status: 200,
		force: Boolean(force)
	};

	if(route.wildcard) {
		entry.from += '*';
		entry.to += ':splat';
	}

	let entryStr = '[[redirects]]\n' + (Object.entries(entry).map(([k, v]) => `  ${k} = ${JSON.stringify(v)}`).join('\n'));

	if(headers) {
		const headersEntries = Object.entries(headers).map(([k, v]) => `${k} = ${JSON.stringify(v)}`);
		entryStr += `\n  headers = {${headersEntries.join(', ')}}`;
	}

	return entryStr;
}

function parseRoute(base, target) {
	const baseUrl = new URL(base, 'http://base.net');

	let prefix = baseUrl.pathname;
	let wildcard = false;

	if(prefix.endsWith('*')) {
		wildcard = true;
		prefix = prefix.slice(0, -1);
	}

	return {prefix, wildcard, target};
}

function devServerRouter(route, req) {
	if(!req.originalUrl.startsWith(route.prefix)) {
		throw new Error('unexpected routed request');
	}

	let {target} = route;

	if(route.wildcard) {
		target += req.originalUrl.slice(route.prefix.length);
	}

	const targetUrl = new URL(target);

	console.log('[netlify-redirects]', {url: req.originalUrl, target});

	req.headers.host = targetUrl.host;

	if(req.headers.origin) {
		req.headers.origin = targetUrl.origin;
	}

	return targetUrl.toString();
}

function fixNetlifyRedirects(webpack, proxy) {
	const netlifyRedirects = [];
	const devServerProxy = {};

	for(let [base, dest] of Object.entries(proxy)) {
		let headers = null;

		if(typeof(dest) === 'object') {
			headers = dest.headers;
			dest = dest.dest;
		}

		const route = parseRoute(base, dest);

		devServerProxy[route.prefix] = {
			headers,
			prefix: route.prefix,
			target: route.target,
			xfwd: true,
			ignorePath: true,
			ws: true,
			router: req => devServerRouter(route, req)
		};

		netlifyRedirects.push(netlifyRedirectEntry(route, headers, true));
	}

	netlifyRedirects.push(netlifyRedirectEntry({prefix: '/*', target: '/index.html'}, null, false));

	webpack.plugins.push(new EmitFilePlugin('netlify.toml', netlifyRedirects.join('\n\n') + '\n'));

	if(!webpack.devServer) {
		webpack.devServer = {};
	}

	if(webpack.devServer.proxy) {
		Object.assign(webpack.devServer.proxy, devServerProxy);
	} else {
		webpack.devServer.proxy = devServerProxy;
	}
}

module.exports = fixNetlifyRedirects;
