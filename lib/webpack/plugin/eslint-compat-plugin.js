const path = require('path');
const {ESLint} = require('eslint');
// Bypass exports restrictions to get the built-in polyfills in the default babel-preset-env configuration
const {BuiltIns} = require(path.resolve(require.resolve('babel-plugin-polyfill-corejs3'), '../built-in-definitions'));

const COREJS_BUILTINS = Object.keys(BuiltIns).filter(k => BuiltIns[k].pure);
const BABEL_POLYFILLS = ['es:all', ...COREJS_BUILTINS];

class EsLintCompatWarning extends Error {
	constructor(message, {file, count}) {
		super(`caniuse: (${count}x) ${message}`);
		this.name = 'EsLintCompatWarning';
		this.file = file;
		this.stack = false;
		this.type = 'lint-error';
	}
}

class EsLintCompatCheckerPlugin {
	constructor({polyfills, browsers} = {}) {
		this.key = 'EsLintCompatCheckerPlugin';
		this.eslint = new ESLint({
			baseConfig: {
				parser: '@babel/eslint-parser',
				extends: ['plugin:compat/recommended'],
				settings: {
					polyfills: [...BABEL_POLYFILLS, ...(polyfills || [])],
					browsers,
					lintAllEsApis: false
				},
				parserOptions: {
					requireConfigFile: false
				}
			},
			resolvePluginsRelativeTo: __dirname,
			useEslintrc: false
		});
	}

	apply(compiler) {
		compiler.hooks.assetEmitted.tapPromise('EsLintCompatWarning', async (file, {content, compilation}) => {
			if(!/\.js$/.test(file)) {
				return;
			}

			const warningMessages = {};
			const report = await this.eslint.lintText(content.toString(), {filePath: file});
			for(const lintResult of report) {
				for(const message of lintResult.messages) {
					if(!message.ruleId || !message.ruleId.startsWith('compat/')) {
						continue;
					}

					if(warningMessages[message.message]) {
						warningMessages[message.message]++;
					} else {
						warningMessages[message.message] = 1;
					}
				}
			}

			for(const [message, count] of Object.entries(warningMessages)) {
				compilation.warnings.push(new EsLintCompatWarning(message, {count, file}));
			}
		});
	}
}

module.exports = EsLintCompatCheckerPlugin;
