const path = require('path');
const webpack = require('webpack');

const PLUGIN_NAME = 'EmitFile';
const PROCESS_STAGE = webpack.Compilation.PROCESS_ASSETS_STAGE_ADDITIONAL;

class EmitFilePlugin {
	constructor(filename, content) {
		this.filename = filename;
		this.content = content;
	}

	apply(compiler) {
		compiler.hooks.thisCompilation.tap(PLUGIN_NAME, compilation => {
			compilation.hooks.processAssets.tapPromise({name: PLUGIN_NAME, stage: PROCESS_STAGE}, async () => {
				compilation.emitAsset(
					path.relative('.', this.filename),
					this.content instanceof webpack.sources.Source ? this.content : new webpack.sources.RawSource(this.content)
				);
			});
		});
	}
}

module.exports = EmitFilePlugin;
