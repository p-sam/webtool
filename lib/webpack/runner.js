const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');

function callbackToPromise(fn) {
	return new Promise((resolve, reject) => {
		fn((error, result) => {
			if(error) {
				reject(error);
				return;
			}

			resolve(result);
		});
	});
}

class WebpackRunner {
	constructor(config) {
		this.config = config;
		this.compiler = webpack(config);
	}

	async run() {
		const stats = await callbackToPromise(cb => this.compiler.run(cb));
		await callbackToPromise(cb => this.compiler.close(cb));
		return stats;
	}

	watch(statsCb) {
		this.compiler.watch({}, (error, stats) => {
			if(error) {
				throw error;
			}

			statsCb(stats);
		});
	}

	serve({host, port, silent}) {
		return new Promise(resolve => {
			const options = Object.assign(
				{host, port, devMiddleware: {stats: 'none'}},
				this.config.devServer || {},
				{onListening: resolve},
			);
			const server = new WebpackDevServer(this.compiler, options);
			if(silent) {
				server.logger.info = () => {};
			}

			server.listen();
		});
	}
}

module.exports = WebpackRunner;
