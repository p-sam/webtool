function callbackToPromise(fn) {
	return new Promise((resolve, reject) => {
		fn((error, result) => {
			if(error) {
				reject(error);
				return;
			}

			resolve(result);
		});
	});
}

class WebpackRunner {
	constructor(webpack, runtimeConfig) {
		if(runtimeConfig.outputJson && runtimeConfig.useDevServer) {
			throw new Error('JSON output is not supported with the dev-server');
		}

		this.webpack = webpack;
		this.compiler = require('webpack')(webpack);
		this.runtimeConfig = runtimeConfig;
	}

	get useDevServer() {
		return this.runtimeConfig.useDevServer;
	}

	get outputJson() {
		return this.runtimeConfig.outputJson;
	}

	async run() {
		const stats = await callbackToPromise(cb => this.compiler.run(cb));
		await callbackToPromise(cb => this.compiler.close(cb));
		this.dumpStats(stats);
		return !stats.hasErrors();
	}

	watch() {
		this.log('Watching enabled');
		this.compiler.watch({}, (error, stats) => {
			if(error) {
				throw error;
			}

			this.dumpStats(stats);
		});
	}

	serve() {
		const WebpackDevServer = require('webpack-dev-server');

		const options = {
			host: this.runtimeConfig.devServerHost,
			port: this.runtimeConfig.devServerPort,
			devMiddleware: {stats: 'none'},
			...this.webpack.devServer
		};

		const server = new WebpackDevServer(options, this.compiler);

		if(this.runtimeConfig.outputJson) {
			server.logger.info = () => {};
		}

		return server.start();
	}

	log(...args) {
		if(this.runtimeConfig.outputJson) {
			return;
		}

		console.log('[WEBTOOL]', ...args);
	}

	dumpStats(stats) {
		if(this.runtimeConfig.outputJson) {
			console.log(JSON.stringify(stats.toJson()));
		}
	}
}

module.exports = WebpackRunner;
