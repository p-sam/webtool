const path = require('path');
const createPostCSSConfig = require('./postcss.js');

function configureDefines(defines, callback) {
	const defineEnvKey = 'process.env';
	const env = {...process.env};
	for(const k in env) {
		if(Object.prototype.hasOwnProperty.call(env, k)) {
			defines[`${defineEnvKey}.${k}`] = JSON.stringify(env[k]);
		}
	}

	// Remove env as an array to avoid server env vars leaking in the generated assets
	if(defines[defineEnvKey]) {
		for(const k in defines[defineEnvKey]) {
			if(Object.prototype.hasOwnProperty.call(defines[defineEnvKey], k)) {
				defines[`${defineEnvKey}.${k}`] = defines[defineEnvKey][k];
			}
		}
	}

	defines[defineEnvKey] = undefined;

	if(callback) {
		callback(defines);
	}
}

function configureHtmlPlugin(options, {pkg}) {
	options.template = path.resolve(__dirname, '../../../page-template.ejs');
	options.templateParameters = {
		pkg: pkg || {},
		html: {
			title: (pkg && (pkg.productName || pkg.name)) || 'App',
			description: pkg && pkg.description
		}
	};
}

function configureAnalyzerPlugin(options, input) {
	Object.assign(options, {
		analyzerMode: input.analyze || 'disabled',
		analyzerHost: input.analyzeHost,
		analyzerPort: input.analyzePort,
		reportFilename: input.analyzeReportFilename || 'asset_report.html',
		excludeAssets: input.analyzeExcludeAssets || [],
		generateStatsFile: Boolean(input.analyzeStats),
		statsFilename: input.analyzeStats
	});

	if(options.analyzerMode === true) {
		options.analyzerMode = 'server';
	}

	if(options.statsFilename === true) {
		options.statsFilename = 'asset_stats.json';
	}
}

function configureEncore(encore, config, builderOptions) {
	encore.setOutputPath('dist/');
	encore.setPublicPath('/');
	encore.cleanupOutputBeforeBuild();

	encore.enableSourceMaps(!encore.isProduction());
	encore.enableVersioning(encore.isProduction());

	encore.configureBabelPresetEnv(config => {
		config.bugfixes = true;
		config.useBuiltIns = 'usage';
		config.corejs = 3;
		config.targets = config.browserslist;
	});

	encore.disableSingleRuntimeChunk();
	encore.configureManifestPlugin(options => {
		options.filename = 'asset_manifest.json';
		options.writeToFileEmit = false;
	});

	encore.configureDevServerOptions(options => {
		options.allowedHosts = 'all';
		options.client = {webSocketURL: 'auto://0.0.0.0:0/ws'};
	});

	encore.configureImageRule({type: 'asset'});
	encore.configureFontRule({type: 'asset'});

	encore.configureDefinePlugin(defines => configureDefines(defines, encore._ext.definesCallback));

	encore.enablePostCssLoader(options => {
		options.postcssOptions = createPostCSSConfig({
			compatCheck: encore._ext.compatCheckEnabled,
			browserslist: config.browserslist,
			pluginCallback: encore._ext.postCSSPluginsCallback
		});
	});

	encore.addAliases({'@': path.resolve(encore.getContext(), './src')});

	encore._ext.compatCheckEnabled = encore.isProduction();
	encore._ext.eslintCompatPluginOptions = {
		polyfills: [],
		browsers: config.browserslist
	};

	encore._ext.htmlPluginEnabled = true;
	encore._ext.htmlPluginOptions = {};
	configureHtmlPlugin(encore._ext.htmlPluginOptions, config);

	encore._ext.analyzerPluginEnabled = true;
	encore._ext.analyzerPluginOptions = {};
	configureAnalyzerPlugin(encore._ext.analyzerPluginOptions, builderOptions.analyze);

	if(config.appEntry) {
		encore.addEntry('app', config.appEntry);
	}
}

module.exports = configureEncore;
