function createPostCSSConfig({browserslist, pluginCallback, compatCheck}) {
	const plugins = [
		require('postcss-use')({modules: '*'}),
		require('postcss-preset-env')({browsers: browserslist}),
		compatCheck && require('doiuse')({browsers: browserslist})
	];

	if(pluginCallback) {
		return {plugins: [...pluginCallback(), plugins]};
	}

	return {plugins};
}

module.exports = createPostCSSConfig;
