const ConfigLoader = require('../config-loader.js');
const EncoreBuilder = require('./encore/builder.js');
const configureEncore = require('./configs/encore.js');

const DEFAULT_HOST = '127.0.0.1';
const DEFAULT_PORT = 4000;

class WebpackBuilder {
	constructor(options, appEntry) {
		this.options = options;
		this.appEntry = appEntry;
		this.encore = new EncoreBuilder(options);
		this.configLoader = new ConfigLoader(this.encore.getContext(), this.encore.isProduction());
	}

	static fromArgv(argv, cwd) {
		// Swap webtool args scheme: <appEntry> --mode <webpackMode>
		// to encore scheme: <webpackMode> --context <cwd>
		const appEntry = argv._[0];

		const options = {
			host: process.env.HOST || DEFAULT_HOST,
			port: Number(process.env.PORT) || DEFAULT_PORT,
			context: argv.cwd || cwd,
			...argv,
			_: [argv.mode || 'dev']
		};

		options.analyze = {
			analyze: argv.analyze,
			analyzeHost: argv.analyzeHost || process.env.ANALYZE_HOST || argv.host || DEFAULT_HOST,
			analyzePort: Number(argv.analyzePort) || Number(process.env.ANALYZE_PORT) || ((Number(argv.port) || DEFAULT_PORT) + 1),
			analyzeReportFilename: argv.analyzeFilename,
			analyzeExcludeAssets: argv.analyzeExclude,
			analyzeStats: argv.analyzeStats
		};

		return new WebpackBuilder(options, appEntry);
	}

	configureEnv() {
		this.configLoader.configureEnv();
		this.encore.configureEnv();
	}

	async build() {
		const config = await this.configLoader.load();

		if(this.appEntry && this.configLoader.absolutePathFor(this.appEntry) !== this.configLoader.configPath) {
			config.appEntry = this.appEntry;
		}

		configureEncore(this.encore, config, this.options);

		if(config.configureEncore) {
			await config.configureEncore(this.encore.proxy());
		}

		const webpack = await this.encore.generateWebpackConfig();

		if(config.configureWebpack) {
			await config.configureWebpack(webpack, this.encore.isProduction());
		}

		return webpack;
	}
}

module.exports = WebpackBuilder;
