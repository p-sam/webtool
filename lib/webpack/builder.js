const fs = require('fs/promises');
fs.constants = require('fs').constants;

const path = require('path');
const EncoreWebpackConfig = require('@symfony/webpack-encore/lib/WebpackConfig');
const encoreParseRuntime = require('@symfony/webpack-encore/lib/config/parse-runtime');
const encoreValidateConfig = require('@symfony/webpack-encore/lib/config/validator');
const encoreWebpackConfigGenerator = require('@symfony/webpack-encore/lib/config-generator');
const encoreLogger = require('@symfony/webpack-encore/lib/logger');
const {WebpackManifestPlugin} = require('@symfony/webpack-encore/lib/webpack-manifest-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const AssetsPlugin = require('assets-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const ConfigLoader = require('../config-loader.js');
const createPostCSSConfig = require('../postcss-config.js');
const EsLintCompatCheckerPlugin = require('./eslint-compat-plugin.js');

const DEFAULT_HOST = '127.0.0.1';
const DEFAULT_PORT = 4000;

class WebpackBuilder {
	constructor(runtimeConfig, appEntry) {
		this.runtimeConfig = runtimeConfig;
		this.appEntry = appEntry;
		if(this.runtimeConfig.outputJson && this.runtimeConfig.useDevServer) {
			throw new Error('JSON output is not supported with the dev-server');
		}

		this.configLoader = new ConfigLoader(this.runtimeConfig.context, this.isProduction);
	}

	static fromArgv(argv, cwd) {
		// Swap webtool args scheme: <appEntry> --mode <webpackMode>
		// to encore scheme: <webpackMode> --context <cwd>
		const appEntry = argv._[0];
		const runtimeConfig = encoreParseRuntime(
			Object.assign(
				{
					host: process.env.HOST || DEFAULT_HOST,
					port: Number(process.env.PORT) || DEFAULT_PORT,
				},
				argv,
				{
					_: [argv.mode || 'dev'],
					context: cwd,
				},
			),
			cwd,
		);

		Object.assign(runtimeConfig, {
			analyze: argv.analyze,
			analyzeHost: argv.analyzeHost || process.env.ANALYZE_HOST || process.env.HOST || DEFAULT_HOST,
			analyzePort: Number(argv.analyzePort) || Number(process.env.ANALYZE_PORT) || ((Number(process.env.PORT) || DEFAULT_PORT) + 1),
			analyzeReportFilename: argv.analyzeFilename || 'asset_report.html',
			analyzeExcludeAssets: argv.analyzeExclude || [],
			analyzeStats: argv.analyzeStats,
		});

		if(runtimeConfig.analyze === true) {
			runtimeConfig.analyze = 'server';
		}

		if(runtimeConfig.analyzeStats === true) {
			runtimeConfig.analyzeStats = 'asset_stats.json';
		}

		return new WebpackBuilder(runtimeConfig, appEntry);
	}

	get isProduction() {
		return this.runtimeConfig.environment === 'production';
	}

	configureEnv() {
		this.configLoader.configureEnv();
		encoreLogger.reset();
		if(this.runtimeConfig.outputJson) {
			encoreLogger.quiet();
		}
	}

	createDefaultEncoreConfig({customDefines, browserslist, postCSSPlugins}) {
		const encore = new EncoreWebpackConfig(this.runtimeConfig, true);

		encore.setOutputPath('dist/');
		encore.setPublicPath('/');
		encore.cleanupOutputBeforeBuild();

		encore.enableSourceMaps(!this.isProduction);
		encore.enableVersioning(this.isProduction);

		encore.configureBabelPresetEnv(config => {
			config.useBuiltIns = 'usage';
			config.corejs = 3;
			config.targets = browserslist;
		});

		encore.disableSingleRuntimeChunk();
		encore.configureManifestPlugin(options => {
			options.filename = 'asset_manifest.json';
			options.writeToFileEmit = false;
		});

		encore.configureDevServerOptions(options => {
			options.firewall = false;
			options.client.host = undefined;
		});

		encore.configureImageRule({type: 'asset'});
		encore.configureFontRule({type: 'asset'});

		encore.configureDefinePlugin(defines => {
			const defineEnvKey = 'process.env';
			const env = Object.assign({}, process.env);
			for(const k in env) {
				if(Object.prototype.hasOwnProperty.call(env, k)) {
					defines[`${defineEnvKey}.${k}`] = JSON.stringify(env[k]);
				}
			}

			// Remove env as an array to avoid server env vars leaking in the generated assets
			if(defines[defineEnvKey]) {
				for(const k in defines[defineEnvKey]) {
					if(Object.prototype.hasOwnProperty.call(defines[defineEnvKey], k)) {
						defines[`${defineEnvKey}.${k}`] = defines[defineEnvKey][k];
					}
				}
			}

			defines[defineEnvKey] = undefined;

			if(customDefines) {
				Object.assign(defines, customDefines);
			}
		});

		encore.enablePostCssLoader(options => {
			options.postcssOptions = createPostCSSConfig({
				isProduction: this.isProduction,
				browserslist,
				prependPlugins: postCSSPlugins,
			});
		});

		encore.ext = {eslintCompatPluginEnabled: this.isProduction};

		encore.ext.enableManifestPlugin = (value = true) => {
			encore.ext.manifestPluginEnabled = value;
		};

		encore.ext.enableEntryFilesPlugin = (value = true) => {
			encore.ext.entryFilesPluginEnabled = value;
		};

		encore.ext.enableEslintCompatPlugin = (value = true) => {
			encore.ext.eslintCompatPluginEnabled = value;
		};

		encore.ext.configureEntryFilesPlugin = cb => {
			encore.ext.entryFilesPluginCallback = cb;
		};

		encore.ext.configureEntryFilesPlugin(options => {
			options.filename = 'asset_entrypoints.json';
			options.keepInMemory = true;
		});

		encore.ext.configureEntryFilesPlugin = cb => {
			encore.ext.entryFilesPluginCallback = cb;
		};

		encore.ext.configureAnalyzerPlugin = cb => {
			encore.ext.analyzerPluginCallback = cb;
		};

		return encore;
	}

	fixConfig({webtool, encore, webpack}) {
		// Fix baseURL being different in dev-server mode
		if(webpack.output.publicPath !== encore.publicPath) {
			webpack.output.publicPath = encore.publicPath;
		}

		for(let i = 0; i < webpack.plugins.length; i++) {
			const plugin = webpack.plugins[i];
			if(plugin instanceof AssetsPlugin && plugin.options.filename === 'entrypoints.json') {
				if(encore.ext.entryFilesPluginEnabled) {
					encore.ext.entryFilesPluginCallback(plugin.options);
				} else {
					webpack.plugins.splice(i, 1);
				}
			} else if(plugin instanceof WebpackManifestPlugin && !encore.ext.manifestPluginEnabled) {
				webpack.plugins.splice(i, 1);
			}
		}

		if(encore.ext.eslintCompatPluginEnabled) {
			webpack.plugins.push(new EsLintCompatCheckerPlugin({browsers: webtool.browserslist}));
		}

		webpack.plugins.push(new HtmlWebpackPlugin({
			template: path.resolve(__dirname, '../../page-template.ejs'),
			templateParameters: {
				pkg: webtool.pkg || {},
				html: {
					title: webtool.pkg.productName || webtool.pkg.name || 'App',
				},
			},
		}));

		const analyzerOptions = {
			analyzerMode: this.runtimeConfig.analyze || 'disabled',
			analyzerHost: this.runtimeConfig.analyzeHost,
			analyzerPort: this.runtimeConfig.analyzePort,
			reportFilename: this.runtimeConfig.analyzeReportFilename,
			excludeAssets: this.runtimeConfig.analyzeExcludeAssets,
			generateStatsFile: Boolean(this.runtimeConfig.analyzeStats),
			statsFilename: this.runtimeConfig.analyzeStats,
		};

		if(encore.ext.analyzerPluginCallback) {
			encore.ext.analyzerPluginCallback(analyzerOptions);
		}

		webpack.plugins.push(new BundleAnalyzerPlugin(analyzerOptions));

		webpack.resolve.alias['@'] = path.resolve(encore.runtimeConfig.context, './src');
	}

	async build() {
		const config = await this.configLoader.load();

		if(this.appEntry && this.absolutePathFor(this.appEntry) !== this.configLoader.configPath) {
			config.appEntry = this.appEntry;
		}

		const encoreConfig = this.createDefaultEncoreConfig({
			customDefines: config.define,
			browserslist: config.browserslist,
			postCSSPlugins: config.postCSS,
		});

		if(config.appEntry) {
			encoreConfig.addEntry('app', config.appEntry);
		}

		if(config.configureEncore) {
			config.configureEncore(encoreConfig, this.isProduction);
		}

		encoreValidateConfig(encoreConfig);

		const webpackConfig = encoreWebpackConfigGenerator(encoreConfig);

		this.fixConfig({
			webtool: config,
			encore: encoreConfig,
			webpack: webpackConfig,
		});

		if(config.configureWebpack) {
			config.configureWebpack(webpackConfig, this.isProduction);
		}

		return webpackConfig;
	}
}

module.exports = WebpackBuilder;
