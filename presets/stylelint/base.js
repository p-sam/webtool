module.exports = {
	extends: [
		'stylelint-config-sass-guidelines',
	],
	rules: {
		indentation: 'tab',
		'selector-max-id': null,
	},
};
