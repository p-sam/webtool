module.exports = {
	extends: ['stylelint-config-standard'],
	rules: {
		indentation: 'tab',
		'string-quotes': 'single',
		'selector-max-id': null
	}
};
