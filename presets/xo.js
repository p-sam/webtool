module.exports = {
	esnext: true,
	extends: [require.resolve('./xo/js.js')],
	plugins: [],
	extensions: ['vue'],
	rules: {},
	envs: ['browser'],
	overrides: [
		{
			files: ['**/*.vue'],
			extends: [require.resolve('./xo/vue.js')],
			plugins: [],
			rules: {},
		},
	],
};
