const {rules} = require('eslint-config-xo/browser.js');

module.exports = {
	extends: [require.resolve('@d3vc/webtool-xo/presets/js.js')],
	rules: {
		...rules,
		'unicorn/prefer-node-protocol': 'off',
		'node/no-unsupported-features/node-builtins': 'off'
	}
};
