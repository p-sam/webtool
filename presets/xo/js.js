const CONTROL_FLOW_KEYWORDS = ['if', 'for', 'while', 'switch'];

module.exports = {
	rules: {
		'keyword-spacing': ['error', {
			overrides: Object.fromEntries(CONTROL_FLOW_KEYWORDS.map(k => [k, {after: false}])),
		}],
		// Should be enabled again once most packages are ESM ready
		'unicorn/prefer-module': 'off',
	},
};
