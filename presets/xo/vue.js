function getRuleList(presetPath, ruleSet) {
	if(!ruleSet) {
		ruleSet = new Set();
	}

	const preset = require(presetPath);
	for(const i in preset.rules) {
		if(Object.prototype.hasOwnProperty.call(preset.rules, i)) {
			ruleSet.add(i);
		}
	}

	if(preset.extends) {
		getRuleList(preset.extends, ruleSet);
	}

	return ruleSet;
}

function getVueRecommendedLayoutRules() {
	try {
		const layoutRules = getRuleList('eslint-plugin-vue/lib/configs/no-layout-rules');
		const recommended = getRuleList('eslint-plugin-vue/lib/configs/recommended');
		return [...layoutRules].filter(r => recommended.has(r));
	} catch {
		return [];
	}
}

module.exports = {
	extends: 'plugin:vue/base',
	plugins: ['vue'],
	rules: Object.assign(
		Object.fromEntries(getVueRecommendedLayoutRules().map(r => [r, 'error'])),
		{
			'import/no-anonymous-default-export': 'off',
			'unicorn/filename-case': ['error', {case: 'pascalCase'}],
			'vue/html-indent': ['error', 'tab', {alignAttributesVertically: true}],
			'vue/script-indent': ['error', 'tab', {baseIndent: 0, switchCase: 1}],
			'vue/max-attributes-per-line': ['error', {
				singleline: {max: 4, allowFirstLine: true},
				multiline: {max: 1, allowFirstLine: false},
			}],
			indent: 'off',
		},
	),
};
