const HTML_FILE_EXTENSIONS = ['vue', 'htm', 'html'];

module.exports = {
	extends: [require.resolve('./stylelint/base.js')],
	extensions: ['css', ...HTML_FILE_EXTENSIONS],
	ignores: [],
	processors: [],
	plugins: [],
	overrides: [{
		files: HTML_FILE_EXTENSIONS.map(ext => `**/*.${ext}`),
		customSyntax: 'postcss-html'
	}]
};
