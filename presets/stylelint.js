const xoConstants = require('xo/lib/constants');

module.exports = {
	extends: [require.resolve('./stylelint/base.js')],
	extensions: ['vue', 'htm', 'html', 'css', 'less', 'scss', 'sass'],
	ignores: [...xoConstants.DEFAULT_IGNORES],
	processors: [],
	plugins: [],
};
