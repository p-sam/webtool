#!/usr/bin/env node

/* eslint-disable import/no-unassigned-import */

require('v8-compile-cache');
require('loud-rejection')();
require('dotenv').config();

require('../lib/cli-runner.js')(process.argv.slice(2), process.cwd())
	.then(rc => {
		if(rc !== undefined) {
			process.exitCode = rc;
		}
	})
	.catch(error => {
		console.error(error);
		process.exitCode = 1;
	});
